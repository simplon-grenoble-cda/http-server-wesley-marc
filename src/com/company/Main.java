package com.company;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;

public class Main {

    /**
     * This applications is a small HTTP server, listening on the port 3000.
     *
     * The server replies to the request "GET /hello", and returns
     * a JSON response :
     *
     * {
     *     "hello": "world"
     * }
     *
     *
     * @param args
     */
    public static void main(String[] args) throws IOException {
        final int SERVER_PORT = 3000;

        /**
         * Here I configure the HTTP server before launching it
         */

        // 1. Setup the PORT on which the server must run
        HttpServer server = HttpServer.create(new InetSocketAddress(SERVER_PORT), 0);

        // 2. Tells the server to handle requests to "/hello" using the handler instance of
        //    HelloHandler
        server.createContext("/hello", new HelloHandler());
        server.createContext("/games", new GamesHandler());


        /**
         * Now the server is configured, we launch it
         */
        server.start();
    }
}
