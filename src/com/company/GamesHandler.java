package com.company;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class GamesHandler implements HttpHandler {

    private int gameId = 1;
    private Map<Integer, String[]> grid = new HashMap<>();
    private Map<Integer, String> nextRound = new HashMap<>();
    private Map<Integer, String> winner = new HashMap<>();

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {


        String requestMethod = httpExchange.getRequestMethod();

        String query = httpExchange.getRequestURI().getPath();
        System.out.println(query);
        String[] splitUrl = query.split("/");


        if(splitUrl[2].equals("new")) {
            newGame(httpExchange);
        }
        if(isAInt(splitUrl[2])) {
            int id = Integer.parseInt(splitUrl[2]);
            if(requestMethod.equals("GET")) {
                gameStatus(httpExchange, id);
            }
            else if (requestMethod.equals("POST")) {
                playToken(httpExchange, id);
            }
        }

    }

    public void newGame(HttpExchange httpExchange) throws IOException {

        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("gameId", gameId);

        String responseBody = jsonResponse.toString();

        httpExchange.getResponseHeaders().add("Content-type", "application/json");

        httpExchange.sendResponseHeaders(200, responseBody.length());
        OutputStream outputStream = httpExchange.getResponseBody();
        outputStream.write(responseBody.getBytes());
        outputStream.close();

        String[] tabChar = {"0", "0", "0", "0", "0", "0", "0", "0", "0"};
        grid.put(gameId, tabChar);
        nextRound.put(gameId, "A");
        winner.put(gameId, null);

        gameId++;
    }

    public void gameStatus(HttpExchange httpExchange, int id) throws IOException {

        JSONObject jsonResponse = new JSONObject();

        if(!grid.containsKey(id)) {
            String responseBody = jsonResponse.toString();

            httpExchange.sendResponseHeaders(404, responseBody.length());
            OutputStream outputStream = httpExchange.getResponseBody();
            outputStream.write(responseBody.getBytes());
            outputStream.close();
        }
        else {


            jsonResponse.put("gameId", id);
            jsonResponse.put("grid", grid.get(id));

            if(nextRound.get(id) == null) jsonResponse.put("nextRound", JSONObject.NULL);
            else jsonResponse.put("nextRound", nextRound.get(id));

            if(winner.get(id) == null) jsonResponse.put("winner", JSONObject.NULL);
            else jsonResponse.put("winner", winner.get(id));

            String responseBody = jsonResponse.toString();

            httpExchange.getResponseHeaders().add("Content-type", "application/json");

            httpExchange.sendResponseHeaders(200, responseBody.length());
            OutputStream outputStream = httpExchange.getResponseBody();
            outputStream.write(responseBody.getBytes());
            outputStream.close();
        }

    }

    public void playToken(HttpExchange httpExchange, int id) throws IOException {

        StringBuilder sb = new StringBuilder();
        InputStream requestBody = httpExchange.getRequestBody();

        int i;
        while ((i = requestBody.read()) != -1) {
            sb.append((char) i);
        }

        JSONObject jsonInput = new JSONObject(sb.toString());
        int cell = jsonInput.getInt("cell");
        String player = jsonInput.getString("player");

        if(nextRound.get(id).equals(player) && grid.get(id)[cell].equals("0")) {

            grid.get(id)[cell] = player;

            if(nextRound.get(id).equals("A")) nextRound.put(id, "B");
            else nextRound.put(id, "A");

            gameStatus(httpExchange, id);
        }
        else {
            JSONObject jsonResponse = new JSONObject();

       //     if(!nextRound.get(id).equals(player)) {
            if(!grid.get(id)[cell].equals("0")) {
                jsonResponse.put("error", "cell_not_empty");
                jsonResponse.put("message", "La case n'est pas vide");
            }
            else {
                jsonResponse.put("error", "bad_player");
                jsonResponse.put("message", "Ce n'est pas a vous de jouer");
            }

            String responseBody = jsonResponse.toString();
            System.out.println(jsonResponse);
            httpExchange.getResponseHeaders().add("Content-type", "application/json");

            httpExchange.sendResponseHeaders(400, responseBody.length());
            OutputStream outputStream = httpExchange.getResponseBody();
            outputStream.write(responseBody.getBytes());
            outputStream.close();
        }
    }

    public boolean isAInt(String chaine) {
        try {
            Integer.parseInt(chaine);
        } catch (NumberFormatException e){
            return false;
        }

        return true;
    }


    public Map<String, String> queryToMap(String query){
        Map<String, String> result = new HashMap<String, String>();
        for (String param : query.split("&")) {
            String pair[] = param.split("=");
            if (pair.length>1) {
                result.put(pair[0], pair[1]);
            }else{
                result.put(pair[0], "");
            }
        }
        return result;


    }


}
